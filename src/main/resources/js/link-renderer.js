function linkify(inputText) {
    var replacedText,
        replacePattern1,
        replacePattern2,
        replacePattern3;

    // Fixing JRASERVER-11505
    replacePattern1 = /(^|[^\"\>])(\b([A-z0-9]+):[\/\-A-Z0-9\+\&\@\#\%\?\=\~\_\|\!\:\,\.\;]+[\-A-Z0-9+&@#\/\%\=\~\_\|])/gim;
    replacedText = inputText.replace(replacePattern1, '<a href="$2" target="_blank">$2</a>');
    
    // URLs starting with "www." (without // before it, or it'd re-link the ones done above).
    replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
    replacedText = replacedText.replace(replacePattern2, '$1<a href="https://$2" target="_blank">$2</a>');

    // Change email addresses to mailto:: links.
    replacePattern3 = /(^|[^\>\:])(\b([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
    replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$2">$2</a>');

    return replacedText;
}

function replaceText() {
    var elementIDs = ["description-val","issue_actions_container","content-wrapper"];
    var arrayLength = elementIDs.length;
    for (var i = 0; i < arrayLength; i++) {
        var container = document.getElementById(elementIDs[i]);
        if (container !== null) {
            var comments = container.getElementsByTagName("p");
            for (const [key, value] of Object.entries(comments)) {
                value.innerHTML = linkify(value.innerHTML);
            }
        }
    }   
}

JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, function(e, context, reason) {
    replaceText();
});
JIRA.bind(JIRA.Events.ISSUE_REFRESHED, function(e, context, reason) {
    replaceText();
});

AJS.$(document).ready(function() {
    setTimeout(() => {
        replaceText();
    }, 150);
});